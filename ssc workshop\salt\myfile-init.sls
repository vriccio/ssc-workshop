{% if grains.os_family == 'RedHat' %}
  {% set my_cfg = '/etc/myfile.conf' %}
{% elif grains.os_family == 'Debian' %}
  {% set my_cfg = '/etc/tmpfiles.d/myfile.conf' %}
{% endif %}
 
my_conf:
  file.managed:
    - name: {{ my_cfg }}
    - source: salt://myfile/files/myfile.conf
