install_httpd:
  pkg.installed:
    - name: httpd

configure_httpd:
  file.managed:
    - name: /etc/httpd/conf/httpd.conf
    - source: salt://apache/files/httpd.conf
    - template: jinja
    - require:
      - pkg: install_httpd

start_httpd:
  service.running:
    - name: httpd
    - watch:
      - file: configure_httpd
