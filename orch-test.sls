test_runner:
   salt.runner:
     - name: test.arg
     - arg:
       - first
       - second
       - third
     - kwargs:
         one: this is the first kwarg 
         two: this is the second kwarg 
         three: this is the third kwarg
