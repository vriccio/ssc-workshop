apache2:
  pkg.installed

apache2 Service:
  service.running:
    - name: apache2
    - enable: True
    - require:
      - pkg: apache2

/etc/apache2/conf-available/tune_apache.conf:
  file.managed:
    - source: salt://apache/files/tune_apache.conf
    - require:
      - pkg: apache2
      
enable_tune_apache:
  apache_conf.enabled:
    - name: tune_apache
    - require:
      - pkg: apache2
