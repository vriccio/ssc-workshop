import errno
import pprint
import sys
import os
from sseapiclient.tornado import SyncClient

sse_url = 'https://ssc-01a.corp.local'
username = 'root'
password = 'salt'
path = '/eapi/salt/files'
env='base'

client = SyncClient.connect(sse_url, username, password, ssl_validate_cert=False)
content = """api_example:
  salt.state:
    - tgt: '*'
    - tgt_type: glob
    - sls: 
      - example
           """
if env == 'sse':
    print("Not for use with sse enviroment")
    exit(1)
else:
    try:
        client.api.fs.save_file(saltenv=env,path=path,contents=content,content_type=None)
        print("OK")
    except:
        print("Fail")
