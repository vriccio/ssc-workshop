set_pass_history:
  lgpo.set:
    - computer_policy:
        Enforce password history: 24
         
update_gpo:
  cmd.run:
    - name: "gpupdate /force"
