import json
import requests
import urllib3
import time
urllib3.disable_warnings()
 
url = "https://vra83.cmbu.local"
headers = {'Content-Type': 'application/json'}
 
def get_token():
    api_url = '{0}/csp/gateway/am/api/login?access_token'.format(url)
    data =  {
              "username": "user",
              "password": "pass"
            }
    response = requests.post(api_url, headers=headers, data=json.dumps(data), verify=False)
    if response.status_code == 200:
        json_data = json.loads(response.content.decode('utf-8'))
        key = json_data['access_token']
        return key
    else:
        return None
access_key = get_token()
headers1 = {'Content-Type': 'application/json',
            'Authorization': 'Bearer {0}'.format(access_key)}
 
def create_ssc_integration(hostname, username, password, name):
    api_url_base = url
    api_url = '{0}/provisioning/uerp/provisioning/mgmt/endpoints?external'.format(api_url_base)
    data =  {
              "endpointProperties": {
                "hostName": hostname,
                "privateKeyId": username,
                "privateKey": password
              },
              "customProperties": {
                "isExternal": "true"
              },
              "endpointType": "saltstack",
              "associatedEndpointLinks": [],
              "name": name,
              "tagLinks": []
            }
    response = requests.post(api_url, headers=headers1, data=json.dumps(data), verify=False)
    if response.status_code == 200:
        print('Successfully created SSC Integration')
    else:
        print(response.status_code)
        print(response.text)
        return response.status_code
create_ssc_integration("fqdn-of-sscappliance", "root", "salt", "SSC-Integration")
